package cs.mad.flashcards.entities
import androidx.room.*

@Entity
data class FlashcardSet(
    @PrimaryKey(autoGenerate = true) val id: Long?,
    val title: String
)
