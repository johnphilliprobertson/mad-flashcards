package cs.mad.flashcards.entities
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Flashcard(
    @PrimaryKey(autoGenerate = true) val id: Long?,
    val question: String,
    val answer: String
)

