package cs.mad.flashcards.activities

import android.content.Intent
import android.os.Bundle
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import cs.mad.flashcards.adapters.FlashcardAdapter
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.database.FlashcardDatabase
import cs.mad.flashcards.database.FlashcardDatabaseDao
import cs.mad.flashcards.databinding.ActivityFlashcardSetDetailBinding
import cs.mad.flashcards.entities.Flashcard
import kotlinx.coroutines.launch


class FlashcardSetDetailActivity : AppCompatActivity() {
    private lateinit var dao: FlashcardDatabaseDao
    private lateinit var binding: ActivityFlashcardSetDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFlashcardSetDetailBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)


        lifecycleScope.launch {
            dao = FlashcardDatabase.getInstance(applicationContext).fcDao()
            binding.flashcardList.adapter = FlashcardAdapter(
                    dao.getAll(),
                    lifecycleScope,
                    dao
            )
        }


        binding.addFlashcardButton.setOnClickListener {
            lifecycleScope.launch {

                var id = dao.insert(Flashcard(null, "test", "test"))
                (binding.flashcardList.adapter as FlashcardAdapter).addItem(Flashcard(id, "test", "test"))
                binding.flashcardList.smoothScrollToPosition((binding.flashcardList.adapter as FlashcardAdapter).itemCount - 1)
            }
        }

        binding.deleteSetButton.setOnClickListener {
            finish()
        }

        binding.studySetButton.setOnClickListener {
            startActivity(Intent(this, StudySetActivity::class.java))
        }
    }
}