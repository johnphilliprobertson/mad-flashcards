package cs.mad.flashcards.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.database.FlashcardDatabase
import cs.mad.flashcards.databinding.ActivityMainBinding
import cs.mad.flashcards.entities.FlashcardSet
import kotlinx.coroutines.launch
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        lifecycleScope.launch {
            val items = FlashcardDatabase.getInstance(applicationContext).fcsDao().getAll()
            binding.flashcardSetList.adapter = FlashcardSetAdapter(items)
        }

        binding.createSetButton.setOnClickListener {

            (binding.flashcardSetList.adapter as FlashcardSetAdapter).addItem(FlashcardSet(null, "title"))
            binding.flashcardSetList.smoothScrollToPosition((binding.flashcardSetList.adapter as FlashcardSetAdapter).itemCount - 1)
        }

    }

}