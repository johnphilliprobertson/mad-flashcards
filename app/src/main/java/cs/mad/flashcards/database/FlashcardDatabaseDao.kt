package cs.mad.flashcards.database
import androidx.room.*
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardSet

@Dao
interface FlashcardDatabaseDao {

    @Insert
    suspend fun insert(fc: Flashcard) : Long

    @Update
    suspend  fun update(fc: Flashcard)

    @Update
    suspend fun delete(fc: Flashcard)

    @Query("select * from flashcard")
    suspend fun getAll(): List<Flashcard>

    @Query("select * from flashcard where id is :id")
    suspend fun getCurrent(id: Long?): Flashcard

}

@Dao
interface FlashcardSetDatabaseDao {

    @Insert
    suspend fun insert(fc: FlashcardSet)

    @Update
    suspend  fun update(fc: FlashcardSet)

    @Query("select * from flashcardset")
    suspend fun getAll(): List<FlashcardSet>

    @Query("delete from flashcardset")
    suspend fun deleteAll()


}
